<?php

/**
* @file
* Contains \Drupal\ltpcloud\Form\SettingsForm.
*/

namespace Drupal\ltpcloud\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
* Configure settings of LTP Cloud
*/
class SettingsForm extends ConfigFormBase {
	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'ltpcloud_settings';
	}
	
	/**
	* {@inheritdoc}
	*/
	protected function getEditableConfigNames() {
		return ['ltpcloud.general'];
	}
	
	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state) {
		$config=$this->config('ltpcloud.general');
		$form['apikey']=array(
			'#type' => 'textfield',
			'#title' => $this->t('API Key of LTP Cloud'),
			'#description' => $this->t('You may apply an API key at @here.', ['@here'=>Link::fromTextAndUrl($this->t('LTP Cloud'), Url::fromUri('http://www.ltp-cloud.com'))->toString()]),
			'#size' => 50,
			'#default_value' => empty($config->get('apikey'))?'':$config->get('apikey'),
			'#required' => TRUE,
		);
		
		return parent::buildForm($form, $form_state);
	}

	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$this->config('ltpcloud.general')
			->set('apikey', $form_state->getValue('apikey'))
			->save();
			
		parent::submitForm($form, $form_state);
	}
}
?>
